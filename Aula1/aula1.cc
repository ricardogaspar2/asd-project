// Aula pratica 1: produtor-consumidor em C++
//

//#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>

int nt = 2;

class queue {

  int nelems;
  int *elems;
  int head;
  int tail;
  int nactual;
  pthread_mutex_t mutex;
  pthread_cond_t condVariable;
public:

  queue(int n);
  void enqueue(int e);
  int dequeue();
};

queue::queue(int n) {
  nelems = n;
  elems = new int[nelems];
  head = 0;
  tail = 0;
  nactual = 0;
  pthread_mutex_init(&mutex,NULL);
  pthread_cond_init(&condVariable,NULL);
}

void queue::enqueue(int e) {
  pthread_mutex_lock(&mutex);
    while(nactual == 5){
      pthread_cond_wait(&condVariable, &mutex);
    }
    elems[head] = e;
    head = (head + 1) % nelems;
    ++nactual;
    pthread_cond_signal(&condVariable);
  pthread_mutex_unlock(&mutex);
}

int queue::dequeue() {
  pthread_mutex_lock(&mutex);
    pthread_cond_signal(&condVariable);
    while(nactual == 0){
      pthread_cond_wait(&condVariable, &mutex);
    }
    int retval = elems[tail];
    tail = (tail+1) % nelems;
    --nactual;
  pthread_mutex_unlock(&mutex);
  return retval;
}

queue *q;

void *producer(void *arg) {
  int count = 3;
  int i,n;
  for (i=0;i<count;i++) {
    n = 10 * (*(int *)arg)+i;
    q->enqueue(n);
    printf("Produzi %d\n",n);
  }
  return NULL;
}

void *consumer(void *arg) {
  int n;
  while(1) {
    sleep(1);
    n = q->dequeue();
    printf("Consumi %d\n",n);
  }
}

int main(int argc, char *argv[])
{
    int r,i;
    pthread_t th[nt];
    pthread_t consumerThread;


    q = new queue(5);

    //producer thread poll creation
    for (i = 0; i < nt; i++) {
        int *a = new int (i);
        r = pthread_create(&th[i], NULL, producer, (void *) a);
        assert (r == 0); //if false, what does it do?
    }

    r = pthread_create(&consumerThread, NULL, consumer, NULL);
    assert (r == 0);

    for (i = 0; i < nt; i++) {
        pthread_join(th[i], NULL);
    }
   // infinite loop for consumer to run
    while (1) ;  
    return 0;
}
