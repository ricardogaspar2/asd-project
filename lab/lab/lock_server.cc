// the lock server implementation

#include "lock_server.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>

lockStatus::lockStatus(int status, int clt) :  status(status) , callerId(clt), counter(0)
{
    pthread_mutex_init(&mutex,NULL);
    pthread_cond_init(&condVariable,NULL);
}

lock_server::lock_server(class rsm *_rsm):
rsm(_rsm), nacquire (0)
{
    pthread_mutex_init(&mutexLocks,NULL);
    //pthread_cond_init(&condVariableLocks,NULL);
    
    rsm->set_state_transfer(this);
}

lock_protocol::status
lock_server::stat(int clt, lock_protocol::lockid_t lid, int &r)
{
    //if(rsm->amiprimary()){
        lock_protocol::status ret = lock_protocol::OK;
        printf("stat request from clt %d\n", clt);
        r = nacquire;
        return ret;
    //}
    //return lock_protocol::NOENT; //O que deveria colocar aqui?
}

/* Acquire method uses a reentrant mechanism that allows the same client acquire the lock of the same block multiple times.*/
lock_protocol::status
lock_server::acquire(int clt, lock_protocol::lockid_t lid, int &r)
{
    //if(!rsm->amiprimary()){
    //    return lock_protocol::NOENT; //O que deveria colocar aqui?
    //}
    lock_protocol::status ret;
    pthread_mutex_lock(&mutexLocks);
    if(locks.find(lid)==locks.end()){
        //locks.insert(lid, &myLock);
        lockStatus* myLock = new lockStatus(lockStatus::FREE, clt);
        locks[lid] = myLock;
    }
    pthread_mutex_unlock(&mutexLocks);
    
//    printf("clt %d is trying to acquire lid= %llu \n",clt, lid);
    
    pthread_mutex_lock(&(locks[lid]->mutex));
    
//    printf("clt %d acquire lid= %llu, lock is bound to clt= %d\n",clt, lid,locks[lid]->callerId);
    
    // Does not WAIT iff the lock is LOCKED but this is an acquire done by the same callerID
    /*while(locks[lid]->status==lockStatus::LOCKED && clt != locks[lid]->callerId){
        pthread_cond_wait(&(locks[lid]->condVariable), &(locks[lid]->mutex));
    }*/

    if(locks[lid]->status==lockStatus::LOCKED && clt != locks[lid]->callerId){
        ret = lock_protocol::RETRY;
        pthread_mutex_unlock(&(locks[lid]->mutex));
        printf("acquire request from clt %d lid= %llu counter= %d\n failed. CLIENT HAS TO RETRY\n", clt, lid, locks[lid]->counter);
        return ret;
    }

    locks[lid]->status = lockStatus::LOCKED;
    locks[lid]->callerId = clt;
    //printf("\nclt %d acquire lid= %llu before counter= %d\n",clt, lid,locks[lid]->counter);
    locks[lid]->counter++;
    //printf("clt %d acquire lid= %llu after counter= %d\n",clt, lid, locks[lid]->counter);
    
    ret = lock_protocol::OK;
    
    pthread_mutex_unlock(&(locks[lid]->mutex));
    
    printf("acquire request from clt %d lid= %llu counter= %d\n\n", clt, lid, locks[lid]->counter);
    r = nacquire;
    return ret;
}

/* Release lock uses a reentrant mechanism that allows the same client release the lock of the same block
 multiple times. The lock is FREE when the client has done the same amount of releases as acquires.*/
lock_protocol::status
lock_server::release(int clt, lock_protocol::lockid_t lid, int &r)
{
//    printf("clt %d is trying to release lid= %llu \n",clt, lid);
    //if(!rsm->amiprimary()){
    //    return lock_protocol::NOENT; //O que deveria colocar aqui?
    //}
    
    lock_protocol::status ret = lock_protocol::OK;
    
    if(locks[lid]->callerId==clt && locks[lid]->status==lockStatus::LOCKED){
//        printf("\nclt %d release lid= %llu, lock is bound to clt= %d\n",clt, lid,locks[lid]->callerId);
        pthread_mutex_lock(&(locks[lid]->mutex));
        //printf("clt %d release lid= %llu before counter= %d\n",clt, lid, locks[lid]->counter);
        if(locks[lid]->counter > 0){
            locks[lid]->counter--;
        }
        //printf("clt %d release lid = %llu after counter= %d\n",clt, lid,locks[lid]->counter);
        if (locks[lid]->counter == 0) {
            locks[lid]->status = lockStatus::FREE;
            pthread_cond_signal(&(locks[lid]->condVariable));
        }
        pthread_mutex_unlock(&(locks[lid]->mutex));
    }
    else{
        ret = lock_protocol::NOENT;
    }
    
    printf("release request from clt %d lid= %llu counter= %d\n\n", clt, lid, locks[lid]->counter);
    r = nacquire;
    return ret;
}

std::string 
lock_server::marshal_state(){
pthread_mutex_lock(&mutexLocks);
    marshall rep;
    rep << locks.size();
    std::map< lock_protocol::lockid_t, lockStatus * >::iterator iter_lock;
    for (iter_lock = locks.begin(); iter_lock != locks.end(); iter_lock++) {
        lock_protocol::lockid_t lockId = iter_lock->first;
        lockStatus* status = iter_lock->second;
        rep << lockId;
        rep << status->status;
        rep << status->callerId;
        rep << status->counter;
    }
pthread_mutex_unlock(&mutexLocks);
  return rep.str();
}

void 
lock_server::unmarshal_state(std::string state){
    pthread_mutex_lock(&mutexLocks);
    unmarshall rep(state);
    unsigned int locks_size;
    rep >> locks_size;
    for (unsigned int i = 0; i < locks_size; i++) {
        lock_protocol::lockid_t lockId;
        rep >> lockId;
        int status;
        int callerId;
        int counter;
        rep >> status;
        rep >> callerId;
        rep >> counter;
        lockStatus* lstat = new lockStatus(status, callerId);
        lstat->counter = counter;
        locks[lockId] = lstat;
    }
    pthread_mutex_unlock(&mutexLocks);
  // unlock any mutexes
}

