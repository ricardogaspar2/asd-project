// yfs client.  implements FS operations using extent and lock server
#include "yfs_client.h"
#include "extent_client.h"
#include "lock_client.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "lock_client.h"


yfs_client::yfs_client(std::string extent_dst, std::string lock_dst)
{
    ec = new extent_client(extent_dst);
    lockClient = new lock_client(lock_dst);
}

yfs_client::inum
yfs_client::n2i(std::string n)
{
    std::istringstream ist(n);
    unsigned long long finum;
    ist >> finum;
    return finum;
}

bool rrry = false;

yfs_client::inum
yfs_client::new_inum(bool file){
   	printf("[yfs_client::new_inum] generating new inum for a file(1) or dir(0) ?=%d\n", file);
    unsigned int i;
    
    if(!rrry){
        srand(time(NULL));
        rrry= true;
    }

    gen_new_inum:
    i = rand();
    
    if (file){
        i = i | 0x80000000;
    } else {
        i = i & 0x7FFFFFFF;
    }
    printf("[yfs_client::new_inum] try this inum %u\n", i);
    lockClient->acquire(i);
    if(ec->getattr(i, *new extent_protocol::attr) == extent_protocol::OK){
        lockClient->release(i);
        goto gen_new_inum;
    }
    //lockClient->release(i);
    printf("[yfs_client::new_inum] chosen inum %u\n", i);
    return i;
}

int
translateXXStatus(int extent_protocol_value){
    switch (extent_protocol_value) {
        case extent_protocol::OK:
            printf("[yfs_client::translateXXStatus] decided value = %d = OK\n", extent_protocol_value );
            return yfs_client::OK;
        case extent_protocol::RPCERR:
            printf("[yfs_client::translateXXStatus] decided value = %d = RPCERR\n", extent_protocol_value);
            return yfs_client::RPCERR;
        case extent_protocol::NOENT:
            printf("[yfs_client::translateXXStatus] decided value = %d = NOENT\n", extent_protocol_value);
            return yfs_client::NOENT;
        case extent_protocol::IOERR:
            printf("[yfs_client::translateXXStatus] decided value = %d = IOERR\n", extent_protocol_value);
            return yfs_client::IOERR;
        case extent_protocol::FBIG:
            return yfs_client::FBIG;
        default:
            printf("[yfs_client::translateXXStatus] decided DEFAULT value = %d = IOERR\n", extent_protocol_value);
            return yfs_client::IOERR;
    }

}

std::string
yfs_client::filename(inum inum)
{
    printf("[yfs_client::filename] inum %llu\n", inum);
    std::ostringstream ost;
    ost << inum;
    return ost.str();
}

bool
yfs_client::isfile(inum inum)
{
    printf("[yfs_client::isfile] inum %llu\n", inum);
    if(inum & 0x80000000)
        return true;
    return false;
}

bool
yfs_client::isdir(inum inum)
{
    printf("[yfs_client::isdir] inum %llu\n", inum);
    return ! isfile(inum);
}


yfs_client::inum
yfs_client::ilookup(inum di, std::string name){
    printf("[yfs_client::ilookup] parent inum %llu name = %s\n", di, name.c_str());
    std::map<std::string, dirent> dirList;
    //this empty string is not used, but is necessary to call the listdir
    if(listdir(di, dirList, *new std::string)!= OK){
        goto not_found;
    }
    if (dirList.count(name) > 0){
        printf("[yfs_client::ilookup] found file = %s inum =%llu \n", name.c_str(), dirList[name].inum);
        return dirList[name].inum;
    }
    
    
not_found:
    printf("[yfs_client::ilookup] file not found\n");
    return NOENT; //poderá dar problemas se houver um inum == NOENT
}

int
yfs_client::getfile(inum inum, fileinfo &fin)
{
    int r;
    lockClient->acquire(inum);
    
    printf("[yfs_client::getfile] inum %llu\n", inum);
    extent_protocol::attr a;
    if ( (r = ec->getattr(inum, a)) != extent_protocol::OK) {
        goto release;
    }
    
    fin.atime = a.atime;
    fin.mtime = a.mtime;
    fin.ctime = a.ctime;
    fin.size = a.size;
    //printf("[yfs_client::getfile] %llu -> sz %llu\n", inum, fin.size);
    
release:
    lockClient->release(inum);
    return translateXXStatus(r);;
}

int
yfs_client::getdir(inum inum, dirinfo &din)
{
    int r;
    lockClient->acquire(inum);
    
    printf("[yfs_client::getdir] inum %llu\n", inum);
    extent_protocol::attr a;
    if ( (r = ec->getattr(inum, a)) != extent_protocol::OK) {
        goto release;
    }
    
    din.atime = a.atime;
    din.mtime = a.mtime;
    din.ctime = a.ctime;
    
release:
    lockClient->release(inum);
    return translateXXStatus(r);;
}

/* This create is only used to create the root directory. */
int
yfs_client::create(inum inum, std::string n){
    
    lockClient->acquire(inum);
    
    printf("[yfs_client::create] Root inum %llu\n", inum);
    int r = ec->put(inum, n);
    
    
    lockClient->release(inum);
    return translateXXStatus(r);;
}

/* This create method creates a file or directory under another directory.*/
int
yfs_client::create(inum parent_inum, inum &child_inum, std::string filename){
    
    int r;
    std::map<std::string, dirent> parentdirList;
    std::string dirContent;
    
    lockClient->acquire(parent_inum);
    printf("[yfs_client::create] parent_inum %llu child_inum %llu filename=%s \n", parent_inum, child_inum, filename.c_str());
    
    
    if(isfile(parent_inum)){
        r =  extent_protocol::NOENT;
//        printf("[yfs_client::create] parent isfile() goto release lock\n");
        goto release;
    }
    //    printf("[yfs_client::create] before listdir\n");
    if (listdir(parent_inum, parentdirList, dirContent) != OK){
        r =  extent_protocol::NOENT;
//        printf("[yfs_client::create] listdir not OK\n");
        goto release;
    }
    //    printf("[yfs_client::create] after listdir\n");
    
    //if the file/directory with the same name already exists in the parent directory
    if(parentdirList.count(filename) > 0 ){
        r = extent_protocol::OK;
        child_inum = parentdirList[filename].inum;
        printf("[yfs_client::create] already exists in the directory. the correct child inum is %llu == %llu child_inum to return \n", parentdirList[filename].inum, child_inum );
        goto release;
    }

    // if the the inum already exists, genrerate a new one
    lockClient->acquire(child_inum);
    if(ec->getattr(child_inum, *new extent_protocol::attr) == extent_protocol::OK){
        lockClient->release(child_inum);
        printf("[yfs_client::create] inum %llu already exists in the filesystem.\n", child_inum );
    
        yfs_client::inum new_child_inum;
        isfile(child_inum)? new_child_inum = new_inum(true): new_child_inum = new_inum(false);
        child_inum = new_child_inum;
        printf("[yfs_client::create] the new inum is %llu \n", new_child_inum);
        
       //lockClient->acquire(child_inum);  
    }

//    printf("[yfs_client::create] filename %s with inum= %llu created\n", filename.c_str(), child_inum);
    // create empty file/dir
    r = ec->put(child_inum, "");
    
    
    // file or dir with the specified name does not exist in the parent dir
    if(r == extent_protocol::OK){
//        printf("[yfs_client::create] the file does not exist in the directory.\n");
        // add file/dir to the entries of the parent dir
        dirContent.append(filename + "," + yfs_client::filename(child_inum) +";");
        r = ec->put(parent_inum, dirContent);
//        printf("[yfs_client::create] filename %s with inum= %llu added to dir parent_inum=%llu\n", filename.c_str(), child_inum, parent_inum);
    }
    lockClient->release(child_inum);
    
release:
//    printf("[yfs_client::create] lock released r= %d \n", translateXXStatus(r));
    lockClient->release(parent_inum);
    return translateXXStatus(r);
}


int
yfs_client::getdircontent(inum inum, std::string &content){
    //printf("[yfs_client::getdircontent] inum %llu before acquiring the lock\n", inum);
    //sem reentrant lock pára aqui no test4a
    lockClient->acquire(inum);
    
    printf("[yfs_client::getdircontent] inum %llu\n", inum);
    int r = ec->get(inum,content);
    printf("[yfs_client::getdircontent] content %s\n", content.c_str());
    lockClient->release(inum);
    
    return translateXXStatus(r);
}

int
yfs_client::listdir(inum inum, std::vector<dirent> &v){
    
    printf("[yfs_client::listdir] inum %llu\n", inum);
    std::string content;
    int r = yfs_client::getdircontent(inum, content);
    if(r != extent_protocol::OK)
      //  return translateXXStatus(r);
        return r;
    
    dirent currentDirent;
    std::string currentName = "";
    std::string currentInum = "";
    int onName = 1;
    for(unsigned int i = 0; i < content.size(); ++i){
        if(content[i]==','){
            onName = 0;
            continue;
        }
        if(content[i]==';'){
            onName = 1;
            //create a dirent
            currentDirent.name = currentName;
            currentDirent.inum = yfs_client::n2i(currentInum);
            v.push_back(currentDirent);
            currentInum = "";
            currentName = "";
            continue;
        }
        if(onName == 1){
            currentName += content[i];
        }
        else{
            currentInum += content[i];
        }
    }
    return r;
    //return translateXXStatus(r);
    
}

int
yfs_client::listdir(inum inum, std::map<std::string, dirent> &dirList, std::string &dirContent){
    
    printf("[yfs_client::listdir_map] inum %llu\n", inum);
    printf("[yfs_client::listdir_map] dirContent init = %s\n", dirContent.c_str());
    std::string content;
    int r = yfs_client::getdircontent(inum, content);
    dirContent = content;

    if(r != extent_protocol::OK)
        return r;
    
    dirent currentDirent;
    std::string currentName = "";
    std::string currentInum = "";
    int onName = 1;
    for(unsigned int i = 0; i < content.size(); ++i){
        if(content[i]==','){
            onName = 0;
            continue;
        }
        if(content[i]==';'){
            onName = 1;
            //create a dirent
            currentDirent.name = currentName;
            currentDirent.inum = yfs_client::n2i(currentInum);
            dirList[currentName] = currentDirent;
            currentInum = "";
            currentName = "";
            continue;
        }
        if(onName == 1){
            currentName += content[i];
        }
        else{
            currentInum += content[i];
        }
    }printf("[yfs_client::listdir_map] finished! dircontent = %s\n", dirContent.c_str());
    return r;
    
}


int
yfs_client::setattr(inum inum, unsigned int size){
    std::string content;
    std::string newContent;
    int r;
    
    lockClient->acquire(inum);
    
    printf("[yfs_client::setattr]\n");
    if( (r = ec->get(inum, content)) != extent_protocol::OK){
        goto release;
    }
    
    printf("[yfs_client::setattr] original size=%zu\n", content.size());
    // truncate the file
    if(size <= content.size()){
        newContent = content.substr(0, size);
    }
    else{
        newContent = content;
        unsigned long delta = size - content.size();
        for(unsigned int i = 0; i < delta; ++i){
            newContent += '\0';
        }
        //newContent.append(delta,'\0')
    }
    r = ec->put(inum, newContent);
    
release:
    lockClient->release(inum);
    return translateXXStatus(r);
}

int
yfs_client::read(inum inum, unsigned int &size, unsigned int off, std::string &buf){
    //read files and dirs
    char *p;
    std::string content;
    int r;
    
    lockClient->acquire(inum);
    
    printf("[yfs_client::read] size= %d off= %d buf= %s\n", size, off, buf.c_str());
    if ((r = ec->get(inum, content)) != extent_protocol::OK)
        goto release;
    
    if(size > content.size())
        size =  (unsigned int) content.size();
    if(off >= content.size())
        size = 0;
    if (off+size > content.size()) {
        size = (unsigned int) content.size() - off;
    }
    p = new char[size];
    memcpy(p, content.c_str() + off, size);
    buf = std::string(p, size);
    delete p;
    
release:
//    printf("[yfs_client::read] release lock size= %d off= %d buf= %s\n", size, off, buf.c_str());
    lockClient->release(inum);
    return translateXXStatus(r);
}

int
yfs_client::write(inum inum, const char *buf, size_t size, unsigned int off, unsigned int &bytes_written){
    std::string content;
    std::string newContent;
    int r;
    
    lockClient->acquire(inum);
    
    printf("[yfs_client::write] inum %llu off= %d size= %zu\n", inum, off, size);
//    printf("[yfs_client::write] buf=\n%s\n", buf);
    
    if((r = ec->get(inum, content)) != extent_protocol::OK){
        goto release;
    }
    
    
    printf("[yfs_client::write] after getting the file content:\n %s\n", content.c_str());
    
    //The bytes will be written after the size limit of the string
    if(off > content.size()){
        //printf("off > content.size()\n");
        content.append('\0', off - content.size());
        content.append(buf, size);
        //printf("offset >= content.size()\n");
    }
    //The bytes will be written within the file (before the end of the string)
    else{
        std::string substr = std::string((char*) content.c_str(), off);
        //printf("substr está assim: %s", substr.c_str());
        substr.append(buf, size);
        
        long delta = content.size() - (off + size) ;
        //printf("Delta = %lu\n", delta);
        if(delta > 0){
            substr.append((char*)content.c_str() + off + size, delta);
        }
        content = substr;
        //printf("String final está assim: %s", content.c_str());
    }
    
//    printf("[yfs_client::write] after writing file content:\n %s\n", (char*) content.c_str());
    //newContent = std::string(p, size);
    //printf("newContentei\n");
    
    r = ec->put(inum, content);
    bytes_written = (unsigned int) size;
    
    
release:
    lockClient->release(inum);
    return translateXXStatus(r);
}


int
yfs_client::remove(inum parent_inum, std::string name){
    std::map<std::string, dirent> direntries;
    int r = extent_protocol::NOENT;
    
    lockClient->acquire(parent_inum);
    
    printf("[yfs_client::remove] parent_inum %llu\n", parent_inum);
    // parent is a dir
    if (isdir(parent_inum)) {
        //this empty string is not used, but is necessary to call the listdir
        listdir(parent_inum, direntries, *new std::string);
        if (direntries.count(name) > 0) {
            if((r = ec->remove(direntries[name].inum)) != extent_protocol::OK)
                goto release;
            direntries.erase(name);
            // construct parent dir entries
            std::string parent_newcontent;
            for (std::map<std::string,dirent>::iterator it=direntries.begin(); it!=direntries.end(); ++it){
                parent_newcontent.append(it->first+","+ filename(it->second.inum)+";");
            }
            r = ec->put(parent_inum, parent_newcontent);
        }
        
        
    }
release:
    lockClient->release(parent_inum);
    return translateXXStatus(r);
}
