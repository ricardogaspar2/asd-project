// the extent server implementation

#include "extent_server.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

extent_server::extent_server() {}

int
extent_server::gettime(){
    // get time
    time_t t;
    time(&t);
    // convert time to calendar time
    struct tm *timeinfo;
    timeinfo = localtime(&t);
    return timeinfo->tm_sec;
}

int
extent_server::put(extent_protocol::extentid_t id, std::string buf, int &r)
{
    
    printf("[extent_server::put]\n");
    printf("buf: %s\n",buf.c_str());
    if (blocks.count(id) == 0) {
        blocks[id] = buf;

        attributes[id].atime = attributes[id].ctime = attributes[id].mtime = gettime();
        
//        attributes[id].atime = timeinfo->tm_sec;
//        attributes[id].ctime = timeinfo->tm_sec;
//        attributes[id].mtime = timeinfo->tm_sec;
        attributes[id].size = static_cast<int>(buf.size());
        
        return extent_protocol::OK;
    }
    else{
        // this way we have to remove first and then insert to overwrite the contents
        //return extent_protocol::NOENT;
        blocks[id] = buf;
        attributes[id].size = static_cast<int>(buf.size());

        attributes[id].ctime = attributes[id].mtime = gettime();
//        time_t t;
//        struct tm *timeinfo;
//        timeinfo = localtime(&t);
//        attributes[id].ctime = timeinfo->tm_sec;
//        attributes[id].mtime = timeinfo->tm_sec;

        return extent_protocol::OK;
    }
    
}

int extent_server::get(extent_protocol::extentid_t id, std::string &buf)
{
    printf("[extent_server::get]\n");
    printf("buf: %s\n",buf.c_str());
    if (blocks.count(id) == 0) {
        return extent_protocol::NOENT;
    }
    else{
        //std::copy(blocks[id].begin(), blocks[id].end(), buf.begin());
        buf = blocks[id];
        attributes[id].atime = gettime();
    }
    return extent_protocol::OK;
    
    //quando é que retornamos extent_protocol::IOERR ??
}

int extent_server::getattr(extent_protocol::extentid_t id, extent_protocol::attr &a)
{
    printf("[extent_server::getattr]\n");
    printf("id: %llu\n",id);
    // You replace this with a real implementation. We send a phony response
    // for now because it's difficult to get FUSE to do anything (including
    // unmount) if getattr fails.
//    a.size = 0;
//    a.atime = 0;
//    a.mtime = 0;
//    a.ctime = 0;
    //return extent_protocol::OK;
    if (attributes.count(id) == 0) {
        printf("[extent_server::getattr] not found\n");
        return extent_protocol::NOENT;
    }
    else{
        printf("[extent_server::getattr] exists\n");
//        // get time
//        time_t t;
//        time(&t);
//        // convert time to calendar time
//        struct tm *timeinfo;
//        timeinfo = localtime(&t);
        
        attributes[id].atime = gettime();
        a.size = attributes[id].size;
        a.atime = attributes[id].atime;
        a.mtime = attributes[id].mtime;
        a.ctime = attributes[id].ctime;
        
    }
    return extent_protocol::OK;
    
}

int extent_server::remove(extent_protocol::extentid_t id, int &r)
{
    printf("[extent_server::remove]\n");
    printf("id: %llu\n",id);
    // if the key id exists then delete the block and the attributes
    if (blocks.count(id) == 0) {
        printf("[extent_server::remove] not found\n");
        return extent_protocol::NOENT;
    }
    else{
        printf("[extent_server::remove] removed\n");
        blocks.erase(id);
        attributes.erase(id);
    }
    return extent_protocol::OK;}

