#include "paxos.h"
#include "handle.h"
// #include <signal.h>
#include <stdio.h>

// This module implements the proposer and acceptor of the Paxos
// distributed algorithm as described by Lamport's "Paxos Made
// Simple".  To kick off an instance of Paxos, the caller supplies a
// list of nodes, a proposed value, and invokes the proposer.  If the
// majority of the nodes agree on the proposed value after running
// this instance of Paxos, the acceptor invokes the upcall
// paxos_commit to inform higher layers of the agreed value for this
// instance.


bool
operator> (const prop_t &a, const prop_t &b)
{
    return (a.n > b.n || (a.n == b.n && a.m > b.m));
}

bool
operator>= (const prop_t &a, const prop_t &b)
{
    return (a.n > b.n || (a.n == b.n && a.m >= b.m));
}

std::string
print_members(const std::vector<std::string> &nodes)
{
    std::string s;
    s.clear();
    
    printf("Paxos: PrintMembers: nodes size = %u\n", nodes.size());
    
    for (unsigned i = 0; i < nodes.size(); i++) {
        s += nodes[i];
        if (i < (nodes.size()-1))
            s += ",";
    }
    return s;
}

bool isamember(std::string m, const std::vector<std::string> &nodes)
{
    for (unsigned i = 0; i < nodes.size(); i++) {
        if (nodes[i] == m) return 1;
    }
    return 0;
}

bool
proposer::isrunning()
{
    bool r;
    assert(pthread_mutex_lock(&pxs_mutex)==0);
    r = !stable;
    assert(pthread_mutex_unlock(&pxs_mutex)==0);
    return r;
}

// check if the servers in l2 contains a majority of servers in l1
bool
proposer::majority(const std::vector<std::string> &l1,
                   const std::vector<std::string> &l2)
{
    unsigned n = 0;
    
    for (unsigned i = 0; i < l1.size(); i++) {
        if (isamember(l1[i], l2))
            n++;
    }
    return n >= (l1.size() >> 1) + 1; //l1.size() >> 1 = l1.size()/2
}

proposer::proposer(class paxos_change *_cfg, class acceptor *_acceptor,
                   std::string _me)
: cfg(_cfg), acc (_acceptor), me (_me), break1 (false), break2 (false),
stable (true)
{
    assert (pthread_mutex_init(&pxs_mutex, NULL) == 0);
    my_n.n = 0;
    my_n.m = me;
}

void
proposer::setn()
{
    my_n.n = acc->get_n_h().n + 1 > my_n.n + 1 ? acc->get_n_h().n + 1 : my_n.n + 1;
}

bool
proposer::run(int instance, std::vector<std::string> newnodes, std::string newv)
{
    std::vector<std::string> accepts;
    std::vector<std::string> nodes;
    std::vector<std::string> nodes1;
    std::string v;
    bool r = false;
    
    pthread_mutex_lock(&pxs_mutex);
    printf("proposer:::rub start: initiate paxos for %s w. i=%d v=%s stable=%d\n",
           print_members(newnodes).c_str(), instance, newv.c_str(), stable);
    if (!stable) {  // already running proposer?
        printf("proposer::run: already running\n");
        pthread_mutex_unlock(&pxs_mutex);
        return false;
    }
    setn();
    accepts.clear();
    nodes.clear();
    v.clear();
    // Changed by us
    c_nodes = newnodes;
    c_v = newv;
    // Changed by us
    nodes = c_nodes;
    
    if (prepare(instance, accepts, nodes, v)) {
        
        if (majority(c_nodes, accepts)) {
            printf("paxos::manager: received a majority of prepare responses\n");
            
            if (v.size() == 0) {
                v = c_v;
            }
            
            breakpoint1();
            
            nodes1 = accepts;
            accepts.clear();
            accept(instance, accepts, nodes1, v);
            
            if (majority(c_nodes, accepts)) {
                printf("paxos::manager: received a majority of accept responses\n");
                
                breakpoint2();
                
                decide(instance, accepts, v);
                r = true;
            } else {
                printf("paxos::manager: no majority of accept responses\n");
            }
        }
        else {
            
            printf("Size of nodes is %i and size of accepts is %i\n", nodes.size() ,accepts.size());
            printf("paxos::manager: no majority of PRepare responses\n");
        }
    } else {
        printf("paxos::manager: prepare is rejected %d\n", stable);
    }
    stable = true;
    pthread_mutex_unlock(&pxs_mutex);
    return r;
}

bool
proposer::prepare(unsigned instance, std::vector<std::string> &accepts,
                  std::vector<std::string> nodes,
                  std::string &v)
{
    
    paxos_protocol::preparearg a;
    paxos_protocol::prepareres r;
    a.instance = instance; //a minha vista
    a.n = my_n; // the actual n was updated with setn()
    prop_t highest;
    highest.n = 0;
    highest.m.clear();
    
    for (unsigned i = 0; i < nodes.size(); i++) {
        printf("\n\nproposer:prepare: NUMBER NUMBER NUMBER %d \n\n", i); //TO DEBUG
        int ret = rpc_const::timeout_failure;
        handle h(nodes[i]);
        
        //Realese the lock from proposer::run to rpc call
        assert(pthread_mutex_unlock(&pxs_mutex) == 0);

        rpcc *accrpc = h.get_rpcc();
        // In case of bind failure, the value of accrpc is 0
        if ( accrpc != 0)
            ret = accrpc->call(paxos_protocol::preparereq, me, a, r, rpcc::to(1000));
        assert(pthread_mutex_lock(&pxs_mutex) == 0);

        
        
        
        if (ret == paxos_protocol::OK) {
            if(r.accept){
                printf("\nproposer:prepare: r.n_a.n = %u , highest.n = %u\n", r.n_a.n, highest.n);  //TO DEBUG
                if (r.n_a > highest){
                    highest = r.n_a;
                    v = r.v_a;
                }
                printf("proposer:prepare: accept\n"); //TO DEBUG
                accepts.push_back(nodes[i]);
                
            }
            else if(r.oldinstance){
                printf("proposer:prepare: oldinstance\n"); //TO DEBUG
                acc->commit(instance, r.v_a);
                return false;
            }
            
        }
        else {
            printf("proposer:prepare: failed\n");
        }
    }
    
    return true;
}


void
proposer::accept(unsigned instance, std::vector<std::string> &accepts,
                 std::vector<std::string> nodes, std::string v)
{
    paxos_protocol::acceptarg a;
    a.instance = instance;
    a.n = my_n;
    a.v = v;
    int r; // int responce from acceptor true/false
    
    for (unsigned i = 0; i < nodes.size(); i++) {
        int ret = rpc_const::timeout_failure;
        handle h(nodes[i]);
        
        //Realese the lock from proposer::run to rpc call
        assert(pthread_mutex_unlock(&pxs_mutex) == 0);
        rpcc *accrpc = h.get_rpcc();
        // In case of bind failure, the value of accrpc is 0
        if ( accrpc != 0)
            ret = accrpc->call(paxos_protocol::acceptreq, me, a, r, rpcc::to(1000));
        assert(pthread_mutex_lock(&pxs_mutex) == 0);
        
        if (ret == paxos_protocol::OK) {
            if(r == 1){
                printf("proposer:accept: accept\n");
                accepts.push_back(nodes[i]);
                
            } else{
                printf("proposer:accept: failed -> ins: %u, v: %s\n", instance, v.c_str());
            }
            
        }
        
    }
    
}

void
proposer::decide(unsigned instance, std::vector<std::string> accepts,
                 std::string v)
{
    paxos_protocol::decidearg a;
    a.instance = instance; //a minha vista
    a.v = v;
    int r;
    
    for (unsigned i = 0; i < accepts.size(); i++) {
        int ret = rpc_const::timeout_failure;
        handle h(accepts[i]);
        
        
        //Realese the lock from proposer::run to rpc call
        assert(pthread_mutex_unlock(&pxs_mutex) == 0);
        rpcc *accrpc = h.get_rpcc();
        // In case of bind failure, the value of accrpc is 0
        if ( accrpc != 0)
            ret = accrpc->call(paxos_protocol::decidereq, me, a, r, rpcc::to(1000));
        assert(pthread_mutex_lock(&pxs_mutex) == 0);
        
        if (ret == paxos_protocol::OK) {
            printf("proposer:decide: accept\n");
            // Does nothing. Everytihing is OK
        } else{
            printf("proposer:decide: failed\n");
        }
    }
    
}

acceptor::acceptor(class paxos_change *_cfg, bool _first, std::string _me,
                   std::string _value)
: cfg(_cfg), me (_me), instance_h(0)
{
    assert (pthread_mutex_init(&pxs_mutex, NULL) == 0);
    
    n_h.n = 0;
    n_h.m = me;
    n_a.n = 0;
    n_a.m = me;
    v_a.clear();
    
    l = new log (this, me);
    
    if (instance_h == 0 && _first) {
        printf("executou o log \n");
        values[1] = _value;
        l->loginstance(1, _value);
        instance_h = 1;
    }
    
    pxs = new rpcs(atoi(_me.c_str()));
    pxs->reg(paxos_protocol::preparereq, this, &acceptor::preparereq);
    pxs->reg(paxos_protocol::acceptreq, this, &acceptor::acceptreq);
    pxs->reg(paxos_protocol::decidereq, this, &acceptor::decidereq);
}

paxos_protocol::status
acceptor::preparereq(std::string src, paxos_protocol::preparearg a,
                     paxos_protocol::prepareres &r)
{
    // handle a preparereq message from proposer
    pthread_mutex_lock(&pxs_mutex);
    
    if(a.instance <= instance_h){
        /*
         * If a node's acceptor gets an RPC request for an old instance,
         * it should reply to the proposer with a special RPC response
         * (set oldinstance to true).
         * This response informs the calling proposer that it
         * is behind and tells it what value was chosen for that instance.
         */
        r.v_a = values[a.instance];
        r.n_a = n_a;
        r.oldinstance = 1;
        r.accept = 0;
        
        printf("proposer:preparereq: OLD VIEW accept = %d, oldinstance = %d, highest accepted n_a.m = %u n_a.m = %s\n", r.accept, r.oldinstance, n_a.n, n_a.m.c_str()); //TO DEBUG
    }
    else if(a.n.n > n_h.n){
        r.oldinstance = 0;
        r.accept = 1;
        n_h = a.n;
        
        
        l->loghigh(n_h);
        
        r.n_a = n_a;
        r.v_a = v_a;
        printf("proposer:preparereq: NEW HIGHEST SEEN accept = %d, oldinstance = %d, n_h.m = %u n_h.m = %s\n", r.accept, r.oldinstance, n_h.n, n_h.m.c_str()); //TO DEBUG
    }
    //e no caso de não ser nada disto
    else{
        r.oldinstance = 0;
        r.accept = 0;
        
        printf("proposer:preparereq: OTHER CASE accept = %d, oldinstance = %d\n", r.accept, r.oldinstance); //TO DEBUG
    }
    
    
    pthread_mutex_unlock(&pxs_mutex);
    
    return paxos_protocol::OK;
    
}

paxos_protocol::status
acceptor::acceptreq(std::string src, paxos_protocol::acceptarg a, int &r)
{
    
    // handle an acceptreq message from proposer
    /*
     if n >= n_h
     n_a = n
     v_a = v
     reply accept_ok(n)
     */
    pthread_mutex_lock(&pxs_mutex);
    r = 0; //estou a supor que é para retornar o true ou false (o ack ou nack)
    
    if(a.n.n >= n_h.n){
        n_a = a.n;
        v_a = a.v;
        
        //        l->loghigh(n_h);
        printf("acceptor::acceptreq n.n = %u, n.m = %s, v = %s \n",n_a.n, n_a.m.c_str(), v_a.c_str());
        l->logprop(n_a, v_a);
        r = 1; //estou a supor que é para retornar o true ou false (o ack ou nack)
    }
    
    pthread_mutex_unlock(&pxs_mutex);
    return paxos_protocol::OK;
}

paxos_protocol::status
acceptor::decidereq(std::string src, paxos_protocol::decidearg a, int &r)
{
    printf("acceptor::decidereq: src = %s, args a.instance = %u , a.v = %s\n", src.c_str(),a.instance, a.v.c_str());
    // handle an decide message from proposer
    
    //Log is still missing
    
    commit(a.instance, a.v);
    return paxos_protocol::OK;
}

void
acceptor::commit_wo(unsigned instance, std::string value)
{
    //assume pxs_mutex is held
    printf("acceptor::commit: instance = %d has v = %s\n", instance, value.c_str());
    if (instance > instance_h) {
        printf("commit: highestaccepteinstance = %d\n", instance);
        values[instance] = value;
        l->loginstance(instance, value);
        instance_h = instance;
        n_h.n = 0;
        n_h.m = me;
        n_a.n = 0;
        n_a.m = me;
        v_a.clear();
        if (cfg) {
            pthread_mutex_unlock(&pxs_mutex);
            cfg->paxos_commit(instance, value);
            pthread_mutex_lock(&pxs_mutex);
        }
    }
}

void
acceptor::commit(unsigned instance, std::string value)
{
    pthread_mutex_lock(&pxs_mutex);
    commit_wo(instance, value);
    pthread_mutex_unlock(&pxs_mutex);
}

std::string
acceptor::dump()
{
    return l->dump();
}

void
acceptor::restore(std::string s)
{
    l->restore(s);
    l->logread();
}



// For testing purposes

// Call this from your code between phases prepare and accept of proposer
void
proposer::breakpoint1()
{
    if (break1) {
        printf("Dying at breakpoint 1!\n");
        exit(1);
    }
}

// Call this from your code between phases accept and decide of proposer
void
proposer::breakpoint2()
{
    if (break2) {
        printf("Dying at breakpoint 2!\n");
        exit(1);
    }
}

void
proposer::breakpoint(int b)
{
    if (b == 3) {
        printf("Proposer: breakpoint 1\n");
        break1 = true;
    } else if (b == 4) {
        printf("Proposer: breakpoint 2\n");
        break2 = true;
    }
}
