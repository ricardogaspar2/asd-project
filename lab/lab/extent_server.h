// this is the extent server

#ifndef extent_server_h
#define extent_server_h

#include <string>
#include <map>
#include "extent_protocol.h"

class extent_server {
 
private:
    int gettime();
protected:
    std::map<extent_protocol::extentid_t, std::string> blocks;
    std::map<extent_protocol::extentid_t, extent_protocol::attr> attributes;
    
public:
    extent_server();
    ~extent_server(){};
    
    int put(extent_protocol::extentid_t id, std::string, int &r);
    int get(extent_protocol::extentid_t id, std::string &buf);
    int getattr(extent_protocol::extentid_t id, extent_protocol::attr &a);
    int remove(extent_protocol::extentid_t id, int &r);
};

#endif