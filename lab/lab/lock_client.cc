// RPC stubs for clients to talk to lock_server

#include "lock_client.h"
#include "rpc.h"
#include "rsm_client.h"
#include <arpa/inet.h>

#include <sstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

lock_client::lock_client(std::string dst)
{
    cl = new rsm_client(dst);
    //rand( (unsigned) time(NULL) );
    random = rand();
    printf("My ID is %d\n", random);
}

int
lock_client::stat(lock_protocol::lockid_t lid)
{
    printf("[lock_client::stat] client ID = %d\n", random);
    int r;
    //int ret = cl->call(lock_protocol::stat, cl->id(), lid, r);
    int ret = cl->call(lock_protocol::stat, random, lid, r);
    //assert (ret == lock_protocol::OK);
    return r;
}

int
lock_client::acquire(lock_protocol::lockid_t lid)
{
    printf("[lock_client::acquire] client ID = %d\n", random);
    int r;
    int ret = lock_protocol::RETRY;
    
    while(ret == lock_protocol::RETRY){
        usleep(50*1000);
        //ret = cl->call(lock_protocol::acquire, cl->id(), lid, r);
        ret = cl->call(lock_protocol::acquire, random, lid, r);
    }
    //cl->call(lock_protocol::acquire, cl->id(), lid, r);
    

    //assert (ret == lock_protocol::OK);
    return r;
}

int
lock_client::release(lock_protocol::lockid_t lid)
{
    printf("[lock_client::release] client ID = %d\n", random);
    int r; 
    //int ret = cl->call(lock_protocol::release, cl->id(), lid, r);
    int ret = cl->call(lock_protocol::release, random, lid, r);
    //assert (ret == lock_protocol::OK);
    return r;
}
