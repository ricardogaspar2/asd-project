// this is the lock server
// the lock client has a similar interface

#ifndef lock_server_h
#define lock_server_h

#include <string>
#include "lock_protocol.h"
#include "lock_client.h"
#include "rpc.h"
#include "rsm.h"

class lockStatus{
public:
    enum lock_status { LOCKED, FREE };
    int status;
    int callerId;
    int counter; // number of locks acquired by the same callerId
    pthread_mutex_t mutex;
    pthread_cond_t condVariable;
    lockStatus(int status, int clt);
    ~lockStatus() {};
};

class lock_server : public rsm_state_transfer{
private:
    class rsm *rsm;    
protected:
    int nacquire;
    std::map<lock_protocol::lockid_t, lockStatus *> locks;
    pthread_mutex_t mutexLocks;
    pthread_cond_t condVariableLocks;
    
public:
    lock_server(class rsm *rsm=0);
    ~lock_server() {};
    lock_protocol::status stat(int clt, lock_protocol::lockid_t lid, int &);
    lock_protocol::status acquire(int clt, lock_protocol::lockid_t lid, int &);
    lock_protocol::status release(int clt, lock_protocol::lockid_t lid, int &);
    void unmarshal_state(std::string state);
    std::string marshal_state();
};

#endif 







