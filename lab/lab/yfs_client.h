#ifndef yfs_client_h
#define yfs_client_h

#include <string>
//#include "yfs_protocol.h"
#include "extent_client.h"
#include <vector>
#include "lock_protocol.h"
#include "lock_client.h"
#include <map>

class yfs_client {
    extent_client *ec;
    lock_client *lockClient;
public:
    
    typedef unsigned long long inum;
    enum xxstatus { OK, RPCERR, NOENT, IOERR, FBIG };
    typedef int status;
    
    struct fileinfo {
        unsigned long long size;
        unsigned long atime;
        unsigned long mtime;
        unsigned long ctime;
    };
    struct dirinfo {
        unsigned long atime;
        unsigned long mtime;
        unsigned long ctime;
    };
    struct dirent {
        std::string name;
        unsigned long long inum;
    };
    
    private:
    static std::string filename(inum);
    static inum n2i(std::string);
    inum new_inum(bool);
    
public:
    
    yfs_client(std::string, std::string);
    
    bool isfile(inum);
    bool isdir(inum);
    inum ilookup(inum di, std::string name);
    
    int getfile(inum, fileinfo &);
    int getdir(inum, dirinfo &);

    int create(inum, std::string);
    int create(inum, inum &, std::string);
    
    int getdircontent(inum, std::string &);
    int listdir(inum, std::vector<dirent> &);
    int listdir(inum, std::map<std::string, dirent> &, std::string &);
    int setattr(inum, unsigned int sz);
    int read(inum, unsigned int &, unsigned int, std::string &);
    int write(inum, const char* buf, size_t, unsigned int, unsigned int &);
    int remove(inum, std::string);
};

#endif 
